import React from "react";
import BodyContent from "./index-sections/new-sections/BodyContent";
import BodyContentAgain from "./index-sections/new-sections/BodyContentAgain";
import BodyContentAgainAgain from "./index-sections/new-sections/BodyContentAgainAgain";
import BodyFooter from "./index-sections/new-sections/BodyFooter";
import BodyHead from "./index-sections/new-sections/BodyHead";
import { JumbotronBaru } from "./index-sections/new-sections/JumbotronBaru";
import { NavbarBaru } from "./index-sections/new-sections/Navbarbaru";
import { FooterBaru } from "./index-sections/new-sections/FooterBaru";

const DaffaIndex = () => {
  return (
    <>
      <NavbarBaru />
      <JumbotronBaru />
      <BodyHead />
      <BodyContent />
      <BodyContent />
      <BodyContentAgain />
      <BodyContentAgainAgain />
      <BodyFooter />
      <FooterBaru />
    </>
  );
};
export default DaffaIndex;
