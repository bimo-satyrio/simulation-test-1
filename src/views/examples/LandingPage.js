/*!

=========================================================
* Paper Kit React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-kit-react

* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/paper-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
  CardImg,
  CardText,
  CardSubtitle,
} from "reactstrap";

// core components
import IndexNavbar from "components/Navbars/IndexNavbar.js";
import Header from "components/Headers/Header.js";
import DemoFooter from "components/Footers/DemoFooter.js";

function LandingPage() {
  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    document.body.classList.add("profile-page");
    return function cleanup() {
      document.body.classList.remove("profile-page");
    };
  });
  return (
    <>
      <IndexNavbar />
      <Header />
      <div className="main">
        <div className="section text-center">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" md="8">
                <h2 className="title">Pilihan Program</h2>
                <h5 className="description">
                  Berikut ini program-program yang tersedia di IELTS Master
                </h5>
                <br />
                <Button
                  className="btn-round"
                  color="info"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                >
                  See Details
                </Button>
              </Col>
            </Row>
            <br />
            <br />
            <Row>
              <Col md="3">
                <div className="info">
                  <div className="icon icon-info">
                    <i className="nc-icon nc-album-2" />
                  </div>
                  <div className="description">
                    <h4 className="info-title">IELTS</h4>
                    <p className="description">
                      Spend your time generating new ideas. You don't have to
                      think of implementing.
                    </p>
                    <Button className="btn-link" color="info" href="#pablo">
                      See more
                    </Button>
                  </div>
                </div>
              </Col>
              <Col md="3">
                <div className="info">
                  <div className="icon icon-info">
                    <i className="nc-icon nc-bulb-63" />
                  </div>
                  <div className="description">
                    <h4 className="info-title">TOEFL</h4>
                    <p>
                      Larger, yet dramatically thinner. More powerful, but
                      remarkably power efficient.
                    </p>
                    <Button className="btn-link" color="info" href="#pablo">
                      See more
                    </Button>
                  </div>
                </div>
              </Col>
              <Col md="3">
                <div className="info">
                  <div className="icon icon-info">
                    <i className="nc-icon nc-chart-bar-32" />
                  </div>
                  <div className="description">
                    <h4 className="info-title">Business</h4>
                    <p>
                      Choose from a veriety of many colors resembling sugar
                      paper pastels.
                    </p>
                    <Button className="btn-link" color="info" href="#pablo">
                      See more
                    </Button>
                  </div>
                </div>
              </Col>
              <Col md="3">
                <div className="info">
                  <div className="icon icon-info">
                    <i className="nc-icon nc-sun-fog-29" />
                  </div>
                  <div className="description">
                    <h4 className="info-title">Academic</h4>
                    <p>
                      Find unique and handmade delightful designs related items
                      directly from our sellers.
                    </p>
                    <Button className="btn-link" color="info" href="#pablo">
                      See more
                    </Button>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <br/>
        <div>
          <Container className="mt-5">
            <h2 class="title">Kelas Online GRATIS!</h2>
            <Row>
              <Col md="6">
                <Row className="mt-4">
                  <Col md="3" className="mt-2">
                    <i class="fas fa-laptop-code">
                      <img
                        width="85"
                        src={require("assets/img/img-laptop.svg")}
                        alt="laptop"
                      />
                    </i>
                  </Col>
                  <Col md="9">
                    <p class="h2 font-weight-bold mt-0">Wisata 1</p>
                    <p class="font-weight-normal mt-1 text-just">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                      aliquet nunc quis turpis fringilla consectetur.
                    </p>
                  </Col>
                  <Col md="3" className="mt-2">
                    <i class="fas fa-laptop-code">
                      <img
                        width="85"
                        src={require("assets/img/img-laptop.svg")}
                        alt="laptop"
                      />
                    </i>
                  </Col>
                  <Col md="9" className="mt-2">
                    <p class="h2 font-weight-bold mt-0">Wisata 2</p>
                    <p class="font-weight-normal mt-1 text-just">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                      aliquet nunc quis turpis fringilla consectetur.
                    </p>
                  </Col>
                  <Col md="3">
                    <i class="fas fa-laptop-code">
                      <img
                        width="85"
                        src={require("assets/img/img-laptop.svg")}
                        alt="laptop"
                      />
                    </i>
                  </Col>
                  <Col md="9" className="mt-2">
                    <p class="h2 font-weight-bold mt-0">Wisata 3</p>
                    <p class="font-weight-normal mt-1 text-just">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                      aliquet nunc quis turpis fringilla consectetur.
                    </p>
                  </Col>
                </Row>
              </Col>
              <Col md="6">
                <img width="500" src={require("assets/img/beach.jpg")} alt="beach" />
              </Col>
            </Row>
          </Container>
        </div>
        <br/>
        <div>
          <Container className="mt-5">
            <p class="h2 font-weight-bold">Berita Terbaru</p>
            <p className="text-muted">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tellus
              justo, maximus eu pharetra ac.
            </p>
            <Row className="mt-3">
              <Col md="4">
                <Card>
                  <CardImg
                    width="100%"
                    src={require("assets/img/Card.jpg")}
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle>Card title</CardTitle>
                    <CardSubtitle>Card subtitle</CardSubtitle>
                    <CardText>
                      Some quick example text to build on the card title and make up
                      the bulk of the card's content.
                    </CardText>
                    <Button>Button</Button>
                  </CardBody>
                </Card>
              </Col>
              <Col md="4">
                <Card>
                  <CardImg
                    width="100%"
                    src={require("assets/img/Card.jpg")}
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle>Card title</CardTitle>
                    <CardSubtitle>Card subtitle</CardSubtitle>
                    <CardText>
                      Some quick example text to build on the card title and make up
                      the bulk of the card's content.
                    </CardText>
                    <Button>Button</Button>
                  </CardBody>
                </Card>
              </Col>
              <Col md="4">
                <Card>
                  <CardImg
                    top
                    width="100%"
                    src={require("assets/img/Card.jpg")}
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle>Card title</CardTitle>
                    <CardSubtitle>Card subtitle</CardSubtitle>
                    <CardText>
                      Some quick example text to build on the card title and make up
                      the bulk of the card's content.
                    </CardText>
                    <Button>Button</Button>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
        <br/>
        <div>
          <Container className="text-center mb-5">
            <p className="h2 font-weight-bold">Mulai Lakukan Perjalan di Sini!</p>
            <p className="h5 text-muted mt-1">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tellus
              justo, maximus eu pharetra ac.
            </p>
            <img
              className="d-block mb-4"
              width="100%"
              height="395"
              src={require("assets/img/beach.jpg")}
              alt="beachh"
            />
            <Button color="primary" size="lg" style={{ width: "250px" }}>
              Show More
            </Button>
          </Container>
        </div>
        <br/>
        <div className="section landing-section">
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" md="8">
                <h2 className="text-center">Keep in touch?</h2>
                <Form className="contact-form">
                  <Row>
                    <Col md="6">
                      <label>Name</label>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="nc-icon nc-single-02" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder="Name" type="text" />
                      </InputGroup>
                    </Col>
                    <Col md="6">
                      <label>Email</label>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="nc-icon nc-email-85" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input placeholder="Email" type="text" />
                      </InputGroup>
                    </Col>
                  </Row>
                  <label>Message</label>
                  <Input
                    placeholder="Tell us your thoughts and feelings..."
                    type="textarea"
                    rows="4"
                  />
                  <Row>
                    <Col className="ml-auto mr-auto" md="4">
                      <Button className="btn-fill" color="danger" size="lg">
                        Send Message
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <DemoFooter />
    </>
  );
}

export default LandingPage;
