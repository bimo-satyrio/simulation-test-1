import React from "react";
import { Container, Row, Col } from "reactstrap";

const BodyContent = () => {
  return (
    <Container className="mt-5">
      <Row className="text-center">
        <Col md="4">
          <div>
            <img src={require("../../../assets/img/1.jpg")} alt="img1" />
          </div>
          <h4>Wisata Alam 1</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            vel pulvinar neque, et bibendum metus.
          </p>
        </Col>
        <Col md="4">
          <img src={require("../../../assets/img/2.jpg")} alt="img2" />
          <h4>Wisata Alam 2</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            vel pulvinar neque, et bibendum metus.
          </p>
        </Col>
        <Col md="4">
          <img src={require("../../../assets/img/3.jpg")} alt="img3" />
          <h4>Wisata Alam 3</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            vel pulvinar neque, et bibendum metus.
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default BodyContent;
