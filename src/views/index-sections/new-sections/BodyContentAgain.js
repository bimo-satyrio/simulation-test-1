import React from "react";
import { Container, Row, Col } from "reactstrap";

const BodyContentAgain = () => {
  return (
    <Container className="mt-5">
      <p class="h2 font-weight-bold">kelas online GRATIS!</p>
      <Row>
        <Col md="6">
          <Row className="mt-4">
            <Col md="3" className="mt-2">
              <i class="fas fa-laptop-code">
                <img
                  width="85"
                  src={require("../../../assets/img/img-laptop.svg")}
                  alt="laptop"
                />
              </i>
            </Col>
            <Col md="9">
              <p class="h2 font-weight-bold mt-0">Wisata 1</p>
              <p class="font-weight-normal mt-1 text-just">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                aliquet nunc quis turpis fringilla consectetur.
              </p>
            </Col>
            <Col md="3" className="mt-2">
              <i class="fas fa-laptop-code">
                <img
                  width="85"
                  src={require("../../../assets/img/img-laptop.svg")}
                  alt="laptop"
                />
              </i>
            </Col>
            <Col md="9" className="mt-2">
              <p class="h2 font-weight-bold mt-0">Wisata 2</p>
              <p class="font-weight-normal mt-1 text-just">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                aliquet nunc quis turpis fringilla consectetur.
              </p>
            </Col>
            <Col md="3">
              <i class="fas fa-laptop-code">
                <img
                  width="85"
                  src={require("../../../assets/img/img-laptop.svg")}
                  alt="laptop"
                />
              </i>
            </Col>
            <Col md="9" className="mt-2">
              <p class="h2 font-weight-bold mt-0">Wisata 3</p>
              <p class="font-weight-normal mt-1 text-just">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                aliquet nunc quis turpis fringilla consectetur.
              </p>
            </Col>
          </Row>
        </Col>
        <Col md="6">
          <img width="500" src={require("../../../assets/img/beach.jpg")} alt="beach" />
        </Col>
      </Row>
    </Container>
  );
};

export default BodyContentAgain;
