import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";

const BodyContentAgainAgain = () => {
  return (
    <Container className="mt-5">
      <p class="h2 font-weight-bold">Berita Terbaru</p>
      <p className="text-muted">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tellus
        justo, maximus eu pharetra ac.
      </p>
      <Row className="mt-3">
        <Col md="4">
          <Card>
            <CardImg
              width="100%"
              src={require("../../../assets/img/Card.jpg")}
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardSubtitle>Card subtitle</CardSubtitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
              <Button>Button</Button>
            </CardBody>
          </Card>
        </Col>
        <Col md="4">
          <Card>
            <CardImg
              width="100%"
              src={require("../../../assets/img/Card.jpg")}
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardSubtitle>Card subtitle</CardSubtitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
              <Button>Button</Button>
            </CardBody>
          </Card>
        </Col>
        <Col md="4">
          <Card>
            <CardImg
              top
              width="100%"
              src={require("../../../assets/img/Card.jpg")}
              alt="Card image cap"
            />
            <CardBody>
              <CardTitle>Card title</CardTitle>
              <CardSubtitle>Card subtitle</CardSubtitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
              <Button>Button</Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
export default BodyContentAgainAgain;
