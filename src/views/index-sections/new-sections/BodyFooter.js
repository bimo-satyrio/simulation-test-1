import React from "react";
import { Container, Button } from "reactstrap";

const BodyFooter = () => {
  return (
    <Container className="text-center mb-5">
      <p className="h2 font-weight-bold">Mulai Lakukan Perjalan di Sini!</p>
      <p className="h5 text-muted mt-1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tellus
        justo, maximus eu pharetra ac.
      </p>
      <img
        className="d-block mb-4"
        width="100%"
        height="395"
        src={require("../../../assets/img/beach.jpg")}
        alt="beachh"
      />
      <Button color="primary" size="lg" style={{ width: "250px" }}>
        Show More
      </Button>
    </Container>
  );
};

export default BodyFooter;
