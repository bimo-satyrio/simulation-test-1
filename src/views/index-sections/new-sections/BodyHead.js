// ! Menggunakan Material UI
import React from "react";
import { Container, Box, Typography } from "@material-ui/core";

const BodyHead = () => {
  return (
    <Container fixed>
      <Typography>
        <Box textAlign="center" fontSize="h4.fontSize">
          Pilihan Wisata
        </Box>
        <Box textAlign="center">
          Berikut ini pilihan-pilihan program yang berada disini
        </Box>
      </Typography>
    </Container>
  );
};

export default BodyHead;
