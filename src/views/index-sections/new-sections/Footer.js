import React from "react";
import { Container, Row, Col } from "reactstrap";
const Footer = () => {
  return (
    <>
      <Container
        fluid
        style={{ backgroundColor: "grey", color: "white" }}
        className="p-4"
      >
        <Row>
          <Col md="3">
            <img
              width="200"
              src={require("../../../assets/img/img-laptop.svg")}
              alt="laptop"
            />
          </Col>

          <Col md="3">
            <p className="h5 font-weight-bold mb-3">Company</p>
            <p className="font-weight-bolder">About Us</p>
            <p className="font-weight-bolder">News Update</p>
            <p className="font-weight-bolder">Login</p>
          </Col>

          <Col md="3">
            <p className="h5 font-weight-bold mb-3">Program</p>
            <p className="font-weight-bolder">Academy</p>
            <p className="font-weight-bolder">Course</p>
          </Col>
          <Col md="3">
            <p className="h5 font-weight-bold mb-3">Support</p>
            <p className="font-weight-bolder">Help</p>
            <p className="font-weight-bolder">F.A.Q</p>
            <p className="font-weight-bolder">Contact US</p>
          </Col>
        </Row>
      </Container>
      <Container
        fluid
        className="p-3"
        style={{ backgroundColor: "blue", color: "white" }}
      >
        <p class="mb-1 mb-sm-0 text-center">
          © 2020. Done Work. All Rights Reserved.
        </p>
      </Container>
    </>
  );
};

export default Footer;
