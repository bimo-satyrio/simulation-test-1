// ! Menggunakan reactstrap
import React from "react";
import { Button, Container, Row, Col, Jumbotron } from "reactstrap";

const HeadContent = () => {
  return (
    <>
      <Container className="mt-1">
        <Row>
          <Col className="pr-0" md="5" sm="12">
            <Jumbotron>
              <h1 className="display-3">Hello, World!</h1>
              <p className="lead">
                This is a simple hero unit, a simple Jumbotron-style component
                for calling extra attention to featured content or information.
              </p>
              <hr className="my-2" />
              <p>
                It uses utility classes for typography and spacing to space
                content out within the larger container.
              </p>
              <p className="lead">
                <Button color="primary">Check This Out!</Button>
              </p>
            </Jumbotron>
          </Col>
          <Col className="pl-0" md="7" sm="12">
            <img src={require("../img/bg-img.jpg")} alt="img-bg" />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default HeadContent;
