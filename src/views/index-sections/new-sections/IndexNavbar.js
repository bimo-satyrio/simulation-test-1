//! Menggunakan reactstrap
import React from "react";

import {
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
} from "reactstrap";

const IndexNavbar = () => {
  return (
    <>
      <Navbar>
        <Container>
          <NavbarBrand>Work Team</NavbarBrand>
          <Nav>
            <NavItem>
              <NavLink href="/">HOME</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/Gallery">GALLERY</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/About">ABOUT</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/Support">SUPPORT US</NavLink>
            </NavItem>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default IndexNavbar;
