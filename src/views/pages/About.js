import IndexNavbar from "../../components/Navbars/IndexNavbar";
import React from "react";
import Footer from "../../components/Footers/DemoFooter";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";

const About = () => {
  return (
    <>
      <IndexNavbar />
      <Container className="mt-5 text-center font-weight-bold">
        <p className="h1 mb-3">Serunya ikut kelas disini!</p>
        <Row className="mt-2 mb-5">
          <Col md="3">
            <img
              src="https://i.ytimg.com/an_webp/p-ce1YwZW68/mqdefault_6s.webp?du=3000&sqp=COOl9PsF&rs=AOn4CLCoHWZj_z9b2uKbGzDKPG4r1CMAhQ"
              alt="..."
            />
            <h4>First Lesson</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse vel pulvinar neque, et bibendum metus.
            </p>
          </Col>
          <Col md="3">
            <img
              src="https://i.ytimg.com/an_webp/p-ce1YwZW68/mqdefault_6s.webp?du=3000&sqp=COOl9PsF&rs=AOn4CLCoHWZj_z9b2uKbGzDKPG4r1CMAhQ"
              alt="..."
            />
            <h4>Second Lesson</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse vel pulvinar neque, et bibendum metus.
            </p>
          </Col>
          <Col md="3">
            <img
              src="https://i.ytimg.com/an_webp/p-ce1YwZW68/mqdefault_6s.webp?du=3000&sqp=COOl9PsF&rs=AOn4CLCoHWZj_z9b2uKbGzDKPG4r1CMAhQ"
              alt="..."
            />
            <h4>Third Lesson</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse vel pulvinar neque, et bibendum metus.
            </p>
          </Col>
          <Col md="3">
            <img
              src="https://i.ytimg.com/an_webp/p-ce1YwZW68/mqdefault_6s.webp?du=3000&sqp=COOl9PsF&rs=AOn4CLCoHWZj_z9b2uKbGzDKPG4r1CMAhQ"
              alt="..."
            />
            <h4>Fourth Lesson</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse vel pulvinar neque, et bibendum metus.
            </p>
          </Col>
        </Row>
        <p className="h1 mt-4 mb-5">Serunya ikut kelas disini!</p>
        <Row>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://yt3.ggpht.com/jYZpYFI86BfR9KOoqJ8BUoBbaeinL2o9XIAAFG8hW7yl9TD8r9IavM19lL6xjgJauNbNMDYyD6SEVgs=s1000-c-fcrop64=1,00000000ffffffff-nd"
                alt="Card image cap"
              />
              <CardBody>
                <Button>Show More</Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>

      <Footer />
    </>
  );
};

export default About;
