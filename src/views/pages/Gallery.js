import IndexNavbar from "../index-sections/new-sections/IndexNavbar";
import React from "react";
import Footer from "../index-sections/new-sections/Footer";
import {
  Card,
  Container,
  Col,
  Row,
  CardText,
  CardImg,
  CardBody,
} from "reactstrap";

const Gallery = () => {
  return (
    <>
      <IndexNavbar />
      <Container className="mt-2">
        <p className="h1">This is a gallery</p>
        <p className="mb-3">
          Improve skills, flexible learning, materials from professionals and
          it’s free!
        </p>
        <Row className="mt-5">
          <Col md="4">
            <Card>
              <CardImg
                top
                width="100%"
                src="https://i.ytimg.com/vi/TzWXqTJk6Qs/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC_JBxQKyYL4FJWCxfVSoM2o5njyQ"
                alt="Card image cap"
              />
              <CardBody className="text-center">
                <CardText>
                  Some quick example text to build on the card title and make up
                  the bulk of the card's content.
                </CardText>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card className="text-center">
              <CardImg
                top
                width="100%"
                src="https://i.ytimg.com/vi/TzWXqTJk6Qs/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC_JBxQKyYL4FJWCxfVSoM2o5njyQ"
                alt="Card image cap"
              />
              <CardBody>
                <CardText>
                  Some quick example text to build on the card title and make up
                  the bulk of the card's content.
                </CardText>
              </CardBody>
            </Card>
          </Col>
          <Col md="4">
            <Card className="text-center">
              <CardImg
                top
                width="100%"
                src="https://i.ytimg.com/vi/TzWXqTJk6Qs/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC_JBxQKyYL4FJWCxfVSoM2o5njyQ"
                alt="Card image cap"
              />
              <CardBody>
                <CardText>
                  Some quick example text to build on the card title and make up
                  the bulk of the card's content.
                </CardText>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
};

export default Gallery;
